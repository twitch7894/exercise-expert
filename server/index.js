const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('./config');
const logger = require('./utils/logger');
const errorHandler = require('./api/libs/errorHandler');
const app = express();

// Database Mongodb
mongoose.connect('mongodb://127.0.0.1:27017/Experta-ART', { useNewUrlParser: true })
mongoose.connection.on('error', () => {
    logger.error('Connection to mongodb failed')
    process.exit(1)
})
mongoose.set('useFindAndModify', false)


// middleware
app.use(bodyParser.json())
app.use(morgan('short', {
    stream: {
       write: message => logger.info(message)
    }
}))
app.use(cors())

// handleError
app.use(errorHandler.processErrorsDB)
if (config.Environment === 'prod') {
  app.use(errorHandler.errorsProduction)
} else {
  app.use(errorHandler.errorsDeveloper)
}

// Route
app.get('/', (req, res) => {
   res.send("hello world")
})

const server = app.listen(config.port, () => {
    logger.info(`Listening in the port ${config.port}.`)
})
  
module.exports = {
    app, server
}