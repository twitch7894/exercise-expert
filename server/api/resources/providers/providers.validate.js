const Joi = require('joi')
const log = require('../../../utils/logger')

const blueprintProvider = Joi.object().keys({
  name: Joi.string().required(),
  cuit: Joi.string().max(11).required(),
  email: Joi.string().required(),
  direction: Joi.string().required(),
  phone: Joi.string().required(),
  owner: Joi.string().required()
})

module.exports = (req, res, next) => {
  let result = Joi.validate(req.body, blueprintProvider, { abortEarly: false, convert: false })
  if (result.error === null) {
    next()
  } else {
    let errorsValidation = result.error.details.reduce((count, error) => {
      return count + `[${error.message}]`
    }, "")

    log.warn('The following Provider did not pass the validation:', req.body, errorsValidation)
    res.status(400).send(`The Provider in the body must specify amount, description and date. Errors in your request: ${errorsValidation}`)
  }
}