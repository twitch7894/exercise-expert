const mongoose = require('mongoose');

const ProviderSchema = new mongoose.Schema({
    name: {
        type: String,
        min: 0,
        required: [true, 'Provider must have an name']
      },
      cuit: {
        type: Number,
        required: [true, 'Provider must have a cuit']
      },
      email: {
        type: String,
        required: [true, 'Provider must have a email']
      },
      direction: {
        type: String,
        required: [true, 'Provider must have an direction']
      },
      phone: {
        type: String,
        required: [true, 'Provider must have an phone']
      },
      owner: {
        type: String,
        required: [true, 'Provider must have an owner']
      }
})

module.exports = mongoose.model('Providers', ProviderSchema)